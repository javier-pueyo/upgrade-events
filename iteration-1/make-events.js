/* 1.1 Añade un botón a tu html con el id btnToClick y en tu javascript añade el 
evento click que ejecute un console log con la información del evento del click

1.2 Añade un evento 'focus' que ejecute un console.log con el valor del input.

1.3 Añade un evento 'input' que ejecute un console.log con el valor del input. */

function runJS() {
    console.log('run');
    const btnToClick = document.createElement('button')
    btnToClick.setAttribute('id', 'btnToClick')
    document.body.append(btnToClick);
    btnToClick.innerText = 'Button';
    btnToClick.addEventListener('click', function(event) {
        console.log('EVENTO:click', event);
    });
    document.addEventListener('focus',function(input){
        console.log(input.target.__proto__);
        console.log(`EVENT:focus. CLASS:${input.target.className}. VALUE:${input.target.value}`);
    }, true);
    document.addEventListener('input', function(input) {
        console.log(`EVENT:input. CLASS:${input.target.className}. VALUE:${input.target.value}`);
    });


}

window.onload = runJS;